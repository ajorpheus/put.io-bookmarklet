// To Convert this RAW JS to a bookmarklet follow these steps:
// 1. First minify the code : http://www.refresh-sf.com/yui/
// 2. Generate Bookmarklet using the minified code : http://benalman.com/code/test/jquery-run-code-bookmarklet/

var files = $("body").find("td[class*='file-name'] a[href*='/file/']").filter(function () {
    var isVideo = $(this).parent().find("div[class='desc ftype']").html().match("video");
    if(!isVideo)
return false;
    else
    return $(this).attr("href").match("/file/[0-9]+");
});

$("#breadcrumb").parent().find("div#newcontent").remove();
$("#breadcrumb").after("<div id='newcontent'><table border=1 width='100%'><tr><td id='aznames'></td><td id='azurls'></td></tr></table></div>");

if (typeof files != "undefined" && files != null && files.length > 0) {
    files.each(function () {
        var fileName = $(this).find("span.fname").text();
        var ext = fileName.split('.').pop();
        //var index = fileName.lastIndexOf(ext);
        //var name = fileName.substring(0,index) + ext;
        
        try {
            $.ajax({
                type: "GET",
                url: this,
                async: false,
                success: function (text) {
                    var firstFileUrl = $(text).find("a[href*='/download?token']").attr("href");
                    var compUrl = firstFileUrl+"#."+ext;
                    var urlHtml = "<a href='" +firstFileUrl + "' >"+ compUrl+" </a>"
                    $("#breadcrumb").parent().find("#azurls").after(urlHtml + "<br>");
                    $("#breadcrumb").parent().find("#aznames").after(fileName + "<br>");
                }
            });
        } catch (err) {}
    });
    
};;
